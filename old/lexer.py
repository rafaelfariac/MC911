#
# 
#
#
#


import sys
import ply.lex as lex

# ========== LIST OF RESERVED WORDS ========== #

reserved = {
	#Reserved Words
	'array': "ARRAY",
	'by': "BY",
	'chars': "CHARS",
	'dcl': "DCL",
	'do': "DO",
	'down': "DOWN",
	'else': "ELSE",
	'elseif': "ELSEIF",
	'end': "END",
	'exit': "EXIT",
	'fi': "FI",
	'for': "FOR",
	'if': "IF",
	'in': "IN",
	'loc': "LOC",
	'type': "TYPE",
	'od': "OD",
	'proc': "PROC",
	'ref': "REF",
	'result': "RESULT",
	'return': "RETURN",
	'returns': "RETURNS",
	'syn': "SYN",
	'then': "THEN",
	'to': "TO",
	'while': "WHILE",
	
	#Predefined Words
	'bool': "BOOL",
	'char': "CHAR",
	'false': "FALSE",
	'int': "INT",
	'length': "LENGTH",
	'lower': "LOWER",
	'null': "NULL",
	'num': "NUM",
	'pred': "PRED",
	'print': "PRINT",
	'read': "READ",
	'succ': "SUCC",
	'true': "TRUE",
	'upper': "UPPER"
}

# ========== LIST OF TOKENS ========== #

tokens = [
	# Operators & Delimiters
	"PLUS", "MINUS", "TIMES", "DIVIDE", "ASSIGN",
	"SEMI", "LPAREN", "RPAREN", "LBRACKET", "RBRACKET",
	"LT", "GT", "LE", "GE", "NE",
	"EQ", "NOT", "AND", "OR", "CONCAT",
	"COMMA", "COLON", "MOD",
	
	# Identifiers
	"ID",
	
	# Literals
	"ICONST", "CCONST", "SCONST"
] + list(reserved.values())


# ========== REGULAR EXPRESSIONS ========== #

#identifiers
def t_ID(t):
	r'[a-zA-Z_][a-zA-Z_0-9]*'
	t.type = reserved.get(t.value, 'ID') #Check for reserved words
	return t

#literals
def t_ICONST(t):
	r'\d+'
	t.value = int(t.value)
	return t
	
def t_CCONST(t):
	r"'(.)'"
	return t
	
def t_SCONST(t):
	r'"(.*?)"'
	t.value = str(t.value)
	return t


# math operators $ delimiters
t_PLUS		= r'\+'
t_MINUS		= r'-'
t_TIMES 	= r'\*'
t_DIVIDE	= r'/'
t_ASSIGN	= r'='
t_SEMI		= r';'
t_LPAREN	= r'\('
t_RPAREN	= r'\)'
t_LBRACKET	= r'\['
t_RBRACKET	= r'\]'
t_LT		= r'<'
t_GT		= r'>'
t_LE		= r'<='
t_GE		= r'>='
t_NE		= r'!='
t_EQ		= r'=='
t_NOT		= r'!'
t_AND		= r'&&'
t_OR		= r'\|\|'
t_CONCAT	= r'&'
t_COMMA		= r','
t_COLON		= r':'
t_MOD		= r'%'

# newline
def t_newline(t):
	r'\n+'
	t.lexer.lineno += len(t.value)
	
# ignore (comma, blank, tab)   
t_ignore = ' \t'
	
# comments
def t_DSCOMMENT(t):
	r'\//.*'
	pass
	
def t_SACOMMENT(t):	
	r'/\*(.*?)\*/'
	pass
	
# error handling
def t_error(t):
	print("%d: unterminated string '%s'" %(t.lexer.lineno, t.value))
	t.lexer.skip(1)


lex.lex()

input_file = open(sys.argv[1], 'r')

lex.input(input_file.read())
while True:
	tok = lex.token()
	if not tok:
		break
	print(tok)
