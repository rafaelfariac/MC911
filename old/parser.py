#	MC911B - 1s2016
# 	Prof. Marcio Machado Pereira
#	Alunos: Rafael Faria Castelao
#			William Nobuaki Ikedo - RA118975
#
#	Projeto 1: Lexical & Syntactic Analysis
#	Arquivo: parser.py


import sys
import ply.yacc as yacc

from lexer import tokens

#precedence = (
#    ('nonassoc', 'LT', 'GT', 'LE', 'GE'),
#    ('left', 'PLUS', 'MINUS'),
#    ('left', 'TIMES', 'DIVIDE'),
#    ('right', 'UMINUS')    #Unary minus operator
#)

# ==================== AST Nodes ====================

# Define a base class for the construction of the AST
class AST(object):
    """
        Base class example for the AST nodes.  Each node is expected to
        define the _fields attribute which lists the names of stored
        attributes.   The __init__() method below takes positional
        arguments and assigns them to the appropriate fields.  Any
        additional arguments specified as keywords are also assigned. """
    _fields = []
    def __init__(self,*args,**kwargs):
        assert len(args) == len(self._fields)
        for name,value in zip(self._fields,args):
            setattr(self,name,value)
        # Assign additional keyword arguments if supplied
        for name,value in kwargs.items():
            setattr(self,name,value)

class Program(AST):
    def __init__(self, stmts):
        _fields = ['stmts']
    
    def children(self):
        nodelist = []
        for i, child in enumerate(self.stmts or []):
            nodelist.append(("stmts[%d]" % i, child))
        return tuple(nodelist)

class ID(AST):
    def __init__(self, identifier, line):
        _fields = ['identifier', 'line']

# ==================== Program rules ====================

# Define a starting rule
start = 'program'

# <program> ::= { <statement> }+
def p_program(p):
    """ program : statement_list """
    p[0] = Program(p[1])

def p_statement_list(p):
    """ statement_list : statement_list statement
                       | statement """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

# <statement> ::= <declaration_statement>
#             | <synonym_statement>
#             | <newmode_statement>
#             | <procedure_statement>
#             | <action_statement>
def p_statement(p):
    """ statement : declaration_statement
                  | synonym_statement
                  | newmode_statement
                  | procedure_statement
                  | action_statement """
    p[0] = p[1]

# ==================== Empty productions ====================
def p_empty(p):
    'empty :'
    pass
             
# ========== Declaration and Identifiers ==========

#<declaration_statement> ::= DCL <declaration_list> ;
def p_declaration_statement(p):
	"""declaration_statement: DCL declaration_list"""
	p[0] = p[1] # CRIAR CLASS
#<declaration_list> ::= <declaration> { , <declaration> }*
def p_declaration_list(p):
	"""declaration_list: declaration
					   | declaration_list declaration"""
	p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]
#<declaration> ::= <identifier_list> <mode> [ <initialization> ]
def p_declaration(p):
	"""declaration: identifier_list mode
				  | identifier_list mode initialization"""
	p[0] = ([p[1]], p[2])
#<initialization> ::=  <assignment_symbol> <expression>
#<identifier_list> ::= <identifier> { , <identifier> }*
#<identifier> ::= [a-zA-Z_][a-zA-Z_0-9]*
def p_identifier(p):
    """ identifier : ID """
    p[0] = ID(p[1], lineno=p.lineno(1))

# ========== Synonym ==========

#<synonym_statement> ::= SYN <synonym_list> ;
#<synonym_list> ::= <synonym_definition> { , <synonym_definition> }*
#<synonym_definition> ::= <identifier_list> [ <mode> ] = <constant_expression>
#<constante_expression> ::= <expression>

# ========== Math Operators and Delimiters ==========

#<operand0> ::=  <operand1>
#            | <operand0> <operator1> <operand1>
def p_operand0(p):
	"""operand0 : operand1
				| operand0 operator1 operand1"""
	if len(p) == 2:
		p[0] = (p[1])
	elif:
		p[0] = (p[0], p[1], p[2])

#<operator1> ::=  <relational_operator>
#             | <membership_operator>
def p_operator1(p):
	"""operator1 : relational_operator
				 | membership_operator"""
	p[0] = p[1]

#<relational_operator> ::=  && | || | == | != | > | >= | < | <=
def p_relational_operator(p):
	"""relational_operator : AND
						   | OR
						   | EQ
						   | NE
						   | GT
						   | GE
						   | LT
						   | LE"""
	p[0] = p[1]

#<membership_operator> ::= IN


#<operand1> ::=  <operand2>
#            | <operand1> <operator2> <operand2>
def p_operand1(p):
	"""operand1 : operand2
				| operand1 operator2 operand2"""
	if p[2] == '+':
		p[0] = p[1] + p[3]
	elif p[2] == '-':
		p[0] = p[1] - p[3]
	elif p[2] == '&':
		p[0] = p[1] + p[3]
	else:
		p[0] = p[1]

#<operator2> ::=  <arithmetic_additive_operator>
#             | <string_concatenation_operator>
def p_operator2(p):
	"""operator2 : arithmetic_additive_operator
				 | string_concatenation_operator"""
	p[0] = p[1]

#<arithmetic_additive_operator> ::= +|-
def p_arithmetic_additive_operator(p):
	"""arithmetic_additive_operator : PLUS
						   | MINUS """
	
	p[0] = p[1]

#<string_concatenation_operator> ::= &
def p_string_concatenation_operator(p):
	"""string_concatenation_operator : CONCAT """
	p[0] = p[1]


# ========== Build the parser ==========
parser = yacc.yacc()

while True:
   try:
       s = raw_input('lya > ')
   except EOFError:
       break
   if not s: continue
   result = parser.parse(s)
   print(result)
