_Alunos: Rafael Faria Castelão e Wiliiam Ikedo_

_Professor: Márcio_

_Site da Matéria: <https://iviarcio.wordpress.com>_

#MC911 - Laboratório de Compiladores

##Resumo das Atividades

**20/03/2016 -** Contém até então o _lexer_ completo.

##Datas de Entrega

Projeto                         | Peso | Data 
:-----------------------------  | :--: | :---:
Lexical and Syntactic Analysis  | 30%  | 06/04
Semantic and Code Generation    | 40%  | 25/05
Virtual Machine Implementation  | 30%  | 29/06