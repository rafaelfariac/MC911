#	MC911B - 1s2016
# 	Prof. Marcio Machado Pereira
#	Alunos: Rafael Faria Castelao
#			William Nobuaki Ikedo - RA118975
#
#	Projeto 1: Lexical & Syntactic Analysis
#	Arquivo: parser.py


import sys
import ply.yacc as yacc

from lexer import tokens

#precedence = (
#    ('nonassoc', 'LT', 'GT', 'LE', 'GE'),
#    ('left', 'PLUS', 'MINUS'),
#    ('left', 'TIMES', 'DIVIDE'),
#    ('right', 'UMINUS')    #Unary minus operator
#)

# ==================== AST Nodes ====================

# Define a base class for the construction of the AST
class AST(object):
    """
        Base class example for the AST nodes.  Each node is expected to
        define the _fields attribute which lists the names of stored
        attributes.   The __init__() method below takes positional
        arguments and assigns them to the appropriate fields.  Any
        additional arguments specified as keywords are also assigned. """
    _fields = []
    def __init__(self,*args,**kwargs):
        assert len(args) == len(self._fields)
        for name,value in zip(self._fields,args):
            setattr(self,name,value)
        # Assign additional keyword arguments if supplied
        for name,value in kwargs.items():
            setattr(self,name,value)

class Program(AST):
    def __init__(self, stmts):
        _fields = ['stmts']
    
    def children(self):
        nodelist = []
        for i, child in enumerate(self.stmts or []):
            nodelist.append(("stmts[%d]" % i, child))
        return tuple(nodelist)

class Declaration_Stmt(AST):
    def __init__(self, declaration_list):
        _fields = ['declaration_list']

    def children(self):
        nodelist = []
        for i, child in enumerate(self.declaration_list or []):
            nodelist.append(("declaration_list[%]" % i, child))
        return tuple(nodelist)

class Synonym_Stmt(AST):
    def __init__(self, synonym_list):
        _fields = ['synonym_list']

    def children(self):
        nodelist = []
        for i, child in enumerate(self.synonym_list or []):
            nodelist.append(("synonym_list[%d]" % i, child))
        return tuple(nodelist)

class Newmode_Stmt(AST):
    def __init__(self, synonym_list):
        _fields = ['newmode_list']
    
    def children(self):
        nodelist = []
        for i, child in enumerate(self.newmode_list or []):
            nodelist.append(("newmode_list[%d]" % i, child))
        return tuple(nodelist)

class ID(AST):
    def __init__(self, identifier, line):
        _fields = ['identifier', 'line']

# ==================== Program rules ====================

# Define a starting rule
start = 'program'

#<program> ::= { <statement> }+
def p_program(p):
    """ program : statement_list """
    p[0] = Program(p[1])

#<statement> ::= <declaration_statement>
#             | <synonym_statement>
#             | <newmode_statement>
#             | <procedure_statement>
#             | <action_statement>
def p_statement(p):
    """ statement : declaration_statement
                  | synonym_statement
                  | newmode_statement
                  | procedure_statement
                  | action_statement """
    p[0] = p[1]

# ==================== Empty productions ====================

def p_empty(p):
    'empty :'
    pass
             
# ==================== Declaration and Identifiers ====================

#<declaration_statement> ::= DCL <declaration_list> ;
def p_declaration_statement(p):
    """ declaration_statement : DCL declaration_list SEMI """
    p[0] = Declaration_Stmt(p[2])

#<declaration_list> ::= <declaration> { , <declaration> }*
def p_declaration_list(p):
    """ declaration_list : declaration
                         | declaration_list COMMA declaration """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

#<declaration> ::= <identifier_list> <mode> [ <initialization> ]
def p_declaration(p):
    """ declaration : identifier_list mode
         | identifier_list mode initialization """
    p[0] = (p[1], p[2]) if len(p) == 3 else (p[1], p[2], p[3])

#<initialization> ::=  <assignment_symbol> <expression>
def p_initialization(p):
    """ initialization : ASSIGN expression """
    p[0] = p[1]

#<identifier_list> ::= <identifier> { , <identifier> }*
def p_identifier_list(p):
    """ identifier_list : identifier
                        | identifier_list COMMA identifier """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]

#<identifier> ::= [a-zA-Z_][a-zA-Z_0-9]*
def p_identifier(p):
    """ identifier : ID """
    p[0] = ID(p[1], lineno=p.lineno(1))

# ==================== Synonym ====================

#<synonym_statement> ::= SYN <synonym_list> ;
def p_synonym_statement(p):
    """ synonym_statement : SYN synonym_list """
    p[0] = Synonym_Stmt(p[2])

#<synonym_list> ::= <synonym_definition> { , <synonym_definition> }*
def p_synonym_list(p):
    """ synonym_list : synonym_definition
                     | synonym_list COMMA synonym_definition """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]

#<synonym_definition> ::= <identifier_list> [ <mode> ] = <constant_expression>
def p_synonym_definition(p):
    """ synonym_definition : identifier_list mode ASSIGN constant_expression
                           | identifier_list ASSIGN constant_expression """
    p[0] = (p[1], p[2], p[4]) if len(p) == 5 else (p[1], p[3])

#<constant_expression> ::= <expression>
def p_constant_expression(p):
    """ constant_expression : expression """
    p[0] = p[1]

# ==================== Newmode Statements ====================

#<newmode_statement> ::= TYPE <newmode_list> ;
def p_newmode_statement(p):
    """ newmode_statement : TYPE newmode_list """
    p[0] = Newmode_Stmt(p[2])

#<newmode_list> ::= <mode_definition> { , <mode_definition> }*
def p_newmode_list(p):
    """ newmode_list : mode_definition
                     | newmode_list COMMA mode_definition """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]

#<mode_definition> ::= <identifier_list> = <mode>
def p_mode_definition(p):
    """ mode_definition : identifier_list ASSIGN mode """
    p[0] = (p[1], p[3])

#<mode> ::=  <mode_name>
#        | <discrete_mode>
#        | <reference_mode>
#        | <composite_mode>
def p_mode(p):
    """ mode : mode_name
             | discrete_mode
             | reference_mode
             | composite_mode """
    p[0] = p[1]

#<discrete_mode> ::=  <integer_mode>
#                 | <boolean_mode>
#                 | <character_mode>
#                 | <discrete_range_mode>
def p_discrete_mode(p):
    """ discrete_mode : integer_mode
                      | boolean_mode
                      | character_mode
                      | discrete_range_mode """
    p[0] = p[1]

#<integer_mode> ::=  INT
def p_integer_mode(p):
    """ integer_mode : INT """
    p[0] = p[1]

#<boolean_mode> ::= BOOL
def p_boolean_mode(p):
    """ boolean_mode : BOOL """
    p[0] = p[1]

#<character_mode> ::= CHAR
def p_character_mode(p):
    """ character_mode : CHAR """
    p[0] = p[1]

#<discrete_range_mode> ::= <discrete_mode_name> ( <literal_range> )
#                       | <discrete_mode> ( <literal_range> )
def p_discrete_range_mode(p):
    """ discrete_range_mode : discrete_mode_name LPAREN literal_range RPAREN
                            | discrete_mode LPAREN literal_range RPAREN """
    p[0] = (p[1], p[3])

#<mode_name> ::= <identifier>
def p_mode_name(p):
    """ mode_name : identifier """
    p[0] = p[1]

#<discrete_mode_name> ::= <identifier>
def p_discrete_mode_name(p):
    """ discrete_mode_name : identifier """
    p[0] = p[1]

#<literal_range> ::= <lower_bound> : <upper_bound>
def p_literal_range(p):
    """ literal_range : lower_bound COLON upper_bound """
    p[0] = (p[1], p[3])

#<lower_bound> ::= <expression>
def p_lower_bound(p):
    """ lower_bound : expression """
    p[0] = p[1]

#<upper_bound> ::= <expression>
def p_upper_bound(p):
    """ upper_bound : expression """
    p[0] = p[1]

#<reference_mode> ::= REF <mode>
def p_reference_mode(p):
    """ reference_mode : REF mode """
    p[0] = p[2]

#<composite_mode> ::= <string_mode> | <array_mode>
def p_composite_mode(p):
    """ composite_mode : string_mode
                       | array_mode """
    p[0] = p[1]

#<string_mode> ::= CHARS LBRACKET <string_length> RBRACKET
def p_string_mode(p):
    """ string_mode : CHARS LBRACKET string_length RBRACKET """
    p[0] = p[3]

#<string_length> ::= <integer_literal>
def p_string_length(p):
    """ string_length : ICONST """
    p[0] = p[1]

#<array_mode> ::= ARRAY LBRACKET <index_mode> { , <index_mode> }* RBRACKET <element_mode>
def p_array_mode(p):
    """ array_mode : ARRAY LBRACKET index_mode_list RBRACKET element_mode """
    p[0] = (p[3], p[5])

def p_index_mode_list(p):
    """ index_mode_list : index_mode
                        | index_mode_list index_mode """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

#<index_mode> ::= <discrete_mode> | <literal_range>
def p_index_mode(p):
    """ index_mode : discrete_mode
                   | literal_range """
    p[0] = p[1]

#<element_mode> ::= <mode>
def p_element_mode(p):
    """ element_mode : mode """
    p[0] = p[1]

# ==================== Location ====================

#<location> ::=  <location_name>
#            | <dereferenced_reference>
#            | <string_element>
#            | <string_slice>
#            | <array_element>
#            | <array_slice>
#            | <call_action>
def p_location(p):
    """ location : identifier
                 | dereferenced_reference
                 | string_element
                 | string_slice
                 | array_element
                 | array_slice
                 | call_action """
    p[0] = p[1]

#<dereferenced_reference> ::= <location> ->
def p_dereferenced_reference(p):
    """ dereferenced_reference : location ARROW """
    p[0] = p[1]

#<string_element> ::= <string_location> LBRACKET <start_element> RBRACKET
def p_string_element(p):
    """ string_element : identifier LBRACKET start_element RBRACKET """
    p[0] = (p[1], p[3])

#<start_element> ::= <integer_expression>
def p_start_element(p):
    """ start_element : ICONST """
    p[0] = p[1]

#<string_slice> ::= <string_location> LBRACKET <left_element> : <right_element> RBRACKET
def p_string_slice(p):
    """ string_slice : identifier LBRACKET left_element COLON right_element RBRACKET """
    p[0] = (p[1], p[3], p[5])

#<string_location> ::= <identifier>
#def p_string_location(p):
#    """ string_location : identifier """
#    p[0] = p[1]

#<left_element> ::= <integer_expression>
def p_left_element(p):
    """ left_element : ICONST """
    p[0] = p[1]

#<right_element> ::= <integer_expression>
def p_right_element(p):
    """ right_element : ICONST """
    p[0] = p[1]

#<array_element> ::= <array_location> LBRACKET <expression_list> RBRACKET
def p_array_element(p):
    """ array_element : array_location LBRACKET expression_list RBRACKET """
    p[0] = (p[1], p[3])

#<expression_list> ::= <expression> { , <expression> }*
def p_expression_list(p):
    """ expression_list : expression
                        | expression_list expression """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

#<array_slice> ::= <array_location> LBRACKET <lower_bound> : <upper_bound> RBRACKET
def p_array_slice(p):
    """ array_slice : array_location LBRACKET lower_bound COLON upper_bound RBRACKET """
    p[0] = (p[1], p[3], p[5])

#<array_location> ::= <location>
def p_array_location(p):
    """ array_location : location """
    p[0] = p[1]

# ==================== Primitive Value ====================

#<primitive_value> ::=  <literal>
#                   | <value_array_element>
#                   | <value_array_slice>
#                   | <parenthesized_expression>
def p_primitive_value(p):
    """ primitive_value : literal
                        | value_array_element
                        | value_array_slice
                        | parenthesized_expression """
    p[0] = p[1]

#<literal> ::=  <integer_literal>
#           | <boolean_literal>
#           | <character_literal>
#           | <empty_literal>
#           | <character_string_literal>
def p_literal(p):
    """ literal : ICONST
                | boolean_literal
                | character_literal 
                | empty_literal
                | character_string_literal """
    p[0] = p[1]

#<integer_literal> ::=  ICONST
#def p_integerl_literal(p):
#    """ integer_literal : ICONST """
#    p[0] = p[1]

#<boolean_literal> ::= FALSE | TRUE
def p_boolean_literal(p):
    """ boolean_literal : TRUE
                        | FALSE """
    p[0] = p[1]

#<character_literal> ::=  '<character>' | '^( <integer_literal> )'
def p_character_literal(p):
    """ character_literal : CCONST """
    p[0] = p[1]

#<empty_literal> ::= NULL
def p_empty_literal(p):
    """ empty_literal : NULL """
    p[0] = None

#<character_string_literal> ::= " { <character> }* "
def p_character_string_literal(p):
    """ character_string_literal : SCONST """
    p[0] = p[1]

# ==================== Value Array Element ====================

#<value_array_element> ::= <array_primitive_value> LBRACKET <expression_list> RBRACKET
def p_value_array_element(p):
    """ value_array_element : array_primitive_value LBRACKET expression_list RBRACKET """
    p[0] = (p[1], p[3])

#<value_array_slice> ::= <array_primitive_value> LBRACKET <lower_element> : <upper_element> RBRACKET
def p_value_array_slice(p):
    """ value_array_slice : array_primitive_value LBRACKET lower_bound COLON upper_bound RBRACKET """
    p[0] = (p[1], p[3], p[5])

#<array_primitive_value> ::= <primitive_value>
def p_array_primitive_value(p):
    """ array_primitive_value : primitive_value """
    p[0] = p[1]

#<parenthesized_expression> ::= ( <expression> )
def p_parenthesized_expression(p):
    """ parenthesized_expression : LPAREN expression RPAREN """
    p[0] = p[2]

#<expression> ::= <operand0> | <conditional_expression>
def p_expression(p):
    """ expression : operand0
                   | conditional_expression """
    p[0] = p[1]

#<conditional_expression> ::=  IF <boolean_expression> <then_expression> <else_expression> FI
#                          | IF <boolean_expression> <then_expression> <elsif_expression> <else_expression> FI
def p_conditional_expression(p):
    """ conditional_expression : IF boolean_expression then_expression else_expression FI
                               | IF boolean_expression then_expression elsif_expression else_expression FI """
    p[0] = (p[2], p[3], p[4]) if len(p) == 6 else (p[2], p[3], p[4], p[5])

#<boolean_expression> ::= <expression>
def p_boolean_expression(p):
    """ boolean_expression : expression """
    p[0] = p[1]

#<then_expression> ::= THEN <expression>
def p_then_expression(p):
    """ then_expression : THEN expression """
    p[0] = p[2]

#<else_expression> ::= ELSE <expression>
def p_else_expression(p):
    """ else_expression : ELSE expression """
    p[0] = p[2]

#<elsif_expression> ::=  ELSIF <boolean_expression> <then_expression>
#                    | elsif_expression ELSIF <boolean_expression> <then_expression>
def p_elsif_expression(p):
    """ elsif_expression : ELSIF boolean_expression then_expression 
                         | elsif_expression ELSIF boolean_expression then_expression """
    p[0] = (p[2], p[3]) if len(p) == 4 else (p[1], p[3], p[4])

# ==================== Math Operators and Delimiters ====================

#<operand0> ::=  <operand1>
#            | <operand0> <operator1> <operand1>
def p_operand0(p):
    """operand0 : operand1
                | operand0 operator1 operand1"""
    if len(p) == 2:
        p[0] = (p[1])
    else:
       	p[0] = (p[0], p[1], p[2])

#<operator1> ::=  <relational_operator>
#             | <membership_operator>
def p_operator1(p):
    """operator1 : relational_operator
                 | membership_operator"""
    p[0] = p[1]

#<relational_operator> ::=  && | || | == | != | > | >= | < | <=
def p_relational_operator(p):
    """relational_operator : AND
                           | OR
                           | EQ
                           | NE
                           | GT
                           | GE
                           | LT
                           | LE """
    p[0] = p[1]

#<membership_operator> ::= IN
def p_membership_operator(p):
    """ membership_operator : IN """
    p[0] = p[1]

#<operand1> ::=  <operand2>
#            | <operand1> <operator2> <operand2>
def p_operand1(p):
    """operand1 : operand2
                | operand1 operator2 operand2"""
    p[0] = p[1] if len(p) == 2 else (p[1], p[2], p[3])

#<operator2> ::=  <arithmetic_additive_operator>
#             | <string_concatenation_operator>
def p_operator2(p):
    """operator2 : arithmetic_additive_operator
                 | string_concatenation_operator"""
    p[0] = p[1]

#<arithmetic_additive_operator> ::= +|-
def p_arithmetic_additive_operator(p):
    """arithmetic_additive_operator : PLUS
                                    | MINUS """
    p[0] = p[1]

#<string_concatenation_operator> ::= &
def p_string_concatenation_operator(p):
    """string_concatenation_operator : CONCAT """
    p[0] = p[1]

#<operand2> ::=  <operand3>
#            | <operand2> <arithmetic_multiplicative_operator> <operand3>
def p_operand2(p):
    """ operand2 : operand3
                 | operand2 arithmetic_multiplicative_operator operand3 """
    p[0] = p[1] if len(p) == 2 else (p[1], p[2], p[3])

#<arithmetic_multiplicative_operator> 
def p_arithmetic_multiplicative_operator(p):
    """ arithmetic_multiplicative_operator : TIMES
                                           | DIVIDE
                                           | MOD """
    p[0] = p[1]

#<operand3> ::=  [ <monadic_operator> ] <operand4>
#            | <integer_literal>
def p_operand3(p):
    """ operand3 : monadic_operator operand4
                 | operand4 """
    p[0] = p[1] if len(p) == 2 else (p[1], p[2])

#<monadic_operator> ::= - | !
def p_monadic_operator(p):
    """ monadic_operator : MINUS
                         | NOT """
    p[0] = p[1]

#<operand4> ::=  <location> | <referenced_location> | <primitive_value>
def p_operand4(p):
    """ operand4 : location
                 | referenced_location 
                 | primitive_value """
    p[0] = p[1]

#<referenced_location> ::= -> <location>
def p_referenced_location(p):
    """ referenced_location : ARROW location """
    p[0] = p[2]

# ==================== Action Statement ====================

#<action_statement> ::= [ <label_id> : ] <action> ;
def p_action_statement(p):
    """ action_statement : label_id COLON action SEMI
                         | action SEMI """
    p[0] = p[1] if len(p) == 3 else (p[1], p[3])

#<label_id> ::= <identifier>
def p_label_id(p):
    """ label_id : identifier """
    p[0] = p[1]

#<action> ::=  <bracketed_action>
#          | <assignment_action>
#          | <call_action>
#          | <exit_action>
#          | <return_action>
#          | <result_action>
def p_action(p):
    """ action : bracketed_action 
               | assignment_action
               | call_action
               | exit_action
               | return_action
               | result_action """
    p[0] = p[1]

#<bracketed_action> ::= <if_action> | <do_action>
def p_bracketed_action(p):
    """ bracketed_action : if_action
                         | do_action """
    p[0] = p[1]

#<assignment_action> ::=    <location> <assigning_operator> <expression>
def p_assignment_action(p):
    """ assignment_action : location assigning_operator expression """
    p[0] = (p[1], p[2], p[3])

#<assigning_operator> ::= [ <closed_dyadic_operator> ] <assignment_symbol>
def p_assigning_operator(p):
    """ assigning_operator : closed_dyadic_operator assignment_symbol
                           | assignment_symbol """
    p[0] = p[1] if len(p) == 2 else (p[1], p[2])

#<closed_dyadic_operator> ::=  <arithmetic_additive_operator>
#                          | <arithmetic_multiplicative_operator>
#                          | <string_concatenation_operator>
def p_closed_dyadic_operator(p):
    """ closed_dyadic_operator : arithmetic_additive_operator
                               | arithmetic_multiplicative_operator
                               | string_concatenation_operator """
    p[0] = p[1]

# ==================== Assignment Symbol ====================

#<assignment_symbol> ::= =
def p_assignment_symbol(p):
    """ assignment_symbol : ASSIGN """
    p[0] = p[1]

#<if_action> ::= IF <boolean_expression> <then_clause> [ <else_clause> ] FI
def p_if_action(p):
    """ if_action : IF boolean_expression then_clause FI
                  | IF boolean_expression then_clause else_clause FI """
    p[0] = (p[2], p[3]) if len(p) == 5 else (p[2], p[3], p[4])

#<then_clause> ::= THEN { <action_statement> }*
def p_then_clause(p):
    """ then_clause : THEN action_statement_list """
    p[0] = p[2]

#<else_clause> ::=  ELSE { <action_statement> }*
#               | ELSIF <boolean_expression> <then_clause> [ <else_clause> ]
def p_else_clause(p):
    """ else_clause : ELSE action_statement_list 
                    | ELSIF boolean_expression then_clause
                    | ELSIF boolean_expression then_clause else_clause """
    if len(p) == 3:
        p[0] = p[2]
    elif len(p) == 5:
        p[0] = (p[2], p[3], p[4])
    else:
        p[0] = (p[2], p[3])


# ==================== Do Action ====================

#<do_action> ::= DO [ <control_part> ; ] { <action_statement> }* OD
def p_do_action(p):
    """ do_action : DO control_part SEMI action_statement_list OD
                  | DO action_statement_list OD """
    p[0] = (p[2], p[4]) if len(p) == 6 else p[2]

def p_action_statement_list(p):
    """ action_statement_list : action_statement
        | action_statement_list action_statement """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

#<control_part> ::=  <for_control> [ <while_control> ]
#                | <while_control>
def p_control_part(p):
    """ control_part : for_control
                     | for_control while_control
                     | while_control """
    p[0] = p[1] if len(p) == 2 else (p[1], p[2])

#<for_control> ::= FOR <iteration>
def p_for_control(p):
    """ for_control : FOR iteration """
    p[0] = p[2]

#<iteration> ::= <step_enumeration> | <range_enumeration>
def p_iteration(p):
    """ iteration : step_enumeration
                  | range_enumeration """
    p[0] = p[1]

#<step_enumeration> ::=  <loop_counter> <assignment_symbol>
#    <start_value> [ <step_value> ] [ DOWN ] <end_value>
def p_step_enumeration(p):
    """ step_enumeration : loop_counter assignment_symbol start_value step_value DOWN end_value
                         | loop_counter assignment_symbol start_value DOWN end_value
                         | loop_counter assignment_symbol start_value step_value end_value
                         | loop_counter assignment_symbol start_value end_value """
    if len(p) == 5:
        p[0] = (p[1], p[2], p[3], p[4])
    elif(p[4] == 'DOWN' and len(p) == 5):
        p[0] = (p[1], p[2], p[3], p[5])
    elif len(p) == 5:
        p[0] = (p[1], p[2], p[3], p[4], p[5])
    else:
        p[0] = (p[1], p[2], p[3], p[4], p[6])

#<loop_counter> ::= <identifier>
def p_loop_counter(p):
    """ loop_counter : identifier """
    p[0] = p[1]

#<start_value> ::= <discrete_expression>
def p_start_value(p):
    """ start_value : discrete_expression """
    p[0] = p[1]

#<step_value> ::= BY <integer_expression>
def p_step_value(p):
    """ step_value : BY ICONST """
    p[0] = p[2]

#<end_value> ::= TO <discrete_expression>
def p_end_value(p):
    """ end_value : TO discrete_expression """
    p[0] = p[2]

#<discrete_expression> ::= <expression>
def p_discrete_expression(p):
    """ discrete_expression : expression """
    p[0] = p[1]

#<range_enumeration> ::= <loop_counter> [ DOWN ] IN <discrete_mode_name>
def p_range_enumeration(p):
    """ range_enumeration : loop_counter DOWN IN discrete_mode_name
                          | loop_counter IN discrete_mode_name """
    p[0] = (p[1], p[3]) if len(p) == 4 else (p[1], p[4])

#<while_control> ::= WHILE <boolean_expression>
def p_while_control(p):
    """ while_control : WHILE boolean_expression """
    p[0] = p[2]

# ==================== Call Action ====================

#<call_action> ::=  <procedure_call> | <builtin_call>
def p_call_action(p):
    """ call_action : procedure_call
                    | builtin_call """
    p[0] = p[1]

#<procedure_call> ::= <procedure_name> ( [ <parameter_list> ] )
def p_procedure_call(p):
    """ procedure_call : identifier LPAREN parameter_list RPAREN
                       | identifier LPAREN RPAREN """
    p[0] = p[1] if len(p) == 4 else (p[1], p[3])

#<parameter_list> ::= <parameter> { , <parameter> }*
def p_parameter_list(p):
    """ parameter_list : parameter
                       | parameter_list parameter """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

#<parameter> ::= <expression>
def p_parameter(p):
    """ parameter : expression """
    p[0] = p[1]

#<procedure_name> ::= <identifier>
#def p_procedure_name(p):
#    """ procedure_name : identifier """
#    p[0] = p[1]

#<exit_action> ::= EXIT label_id
def p_exit_action(p):
    """ exit_action : EXIT label_id """
    p[0] = p[2]

#<return_action> ::= RETURN [ <result> ]
def p_return_action(p):
    """ return_action : RETURN result
                      | RETURN """
    p[0] = p[2] if len(p) == 3 else None

#<result_action> ::= RESULT <result>
def p_result_action(p):
    """ result_action : RESULT result """
    p[0] = p[2]

#<result> ::= <expression>
def p_result(p):
    """ result : expression """
    p[0] = p[1]

#<builtin_call> ::= <builtin_name> ( [ <parameter_list> ] )
def p_builtin_call(p):
    """ builtin_call : builtin_name LPAREN parameter_list RPAREN
                     | builtin_name LPAREN RPAREN """
    p[0] = (p[1], p[3]) if len(p) == 5 else p[1]

#<builtin_name> ::= NUM | PRED | SUCC | UPPER | LOWER | LENGTH | READ | PRINT
def p_builtin_name(p):
    """ builtin_name : NUM
                     | PRED
                     | SUCC
                     | UPPER
                     | LOWER
                     | LENGTH
                     | READ
                     | PRINT """
    p[0] = p[1]

# ==================== Procedure Statement ====================

#<procedure_statement> ::= <label_id> : <procedure_definition> ;
def p_procedure_statement(p):
    """ procedure_statement : label_id COLON procedure_definition SEMI """
    p[0] = (p[1], p[3])

#<procedure_definition> ::=
#    PROC ( [ <formal_parameter_list> ] ) [ <result_spec> ];
#    { <statement> }* END
def p_procedure_definition(p):
    """ procedure_definition : PROC LPAREN formal_parameter_list RPAREN result_spec SEMI statement_list END
                             | PROC LPAREN RPAREN result_spec SEMI statement_list END
                             | PROC LPAREN formal_parameter_list RPAREN SEMI statement_list END
                             | PROC LPAREN RPAREN SEMI statement_list END """
    if len(p) == 9:
        p[0] = (p[3], p[5], p[7])
    elif ( p[3] == 'RPAREN' and len(p) == 8):
        p[0] = (p[4], p[6])
    elif len(p) == 8:
        p[0] = (p[3], p[6])
    else:
        p[0] = p[5]

def p_statement_list(p):
    """ statement_list : statement
        | statement_list statement """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

#<formal_parameter_list> ::= <formal_parameter> { , <formal_parameter> }*
def p_formal_parameter_list(p):
    """ formal_parameter_list : formal_parameter
                              | formal_parameter_list COMMA formal_parameter """
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]

#<formal_parameter> ::= <identifier_list> <parameter_spec>
def p_formal_parameter(p):
    """ formal_parameter : identifier_list parameter_spec """
    p[0] = (p[1], p[2])

#<parameter_spec> ::=  <mode> [ <parameter_attribute> ]
def p_parameter_spec(p):
    """ parameter_spec : mode
                       | mode parameter_attribute """
    p[0] = p[1] if len(p) == 2 else (p[1], p[2])

#<parameter_attribute> ::= LOC
def p_parameter_attribute(p):
    """ parameter_attribute : LOC """
    p[0] = p[1]

#<result_spec> ::= RETURNS ( <mode> [ <result_attribute> ] )
def p_result_spec(p):
    """ result_spec : RETURNS LPAREN mode result_attribute RPAREN
                    | RETURNS LPAREN mode RPAREN """
    p[0] = (p[3], p[4]) if len(p) == 6 else p[3]

#<result_attribute> ::= LOC
def p_result_attribute(p):
    """ result_attribute : LOC """
    p[0] = p[1]

# ==================== Error ====================

def p_error(p):
    print ("Syntax error in input!")


# ========== Build the parser ==========
parser = yacc.yacc()

while True:
    try:
        s = raw_input('lya > ')
    except EOFError:
        break
    if not s: continue
    result = parser.parse(s)
    #print(result)
